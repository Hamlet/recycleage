# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.


## [1.3.3] - 2020-07-15
### Changed

	- Handles' scraps (sticks) reduced to 1 (was 2).
	- Fixed error that broke an item if used on "wrong" nodes, e.g. pick on leaves (thanks Bastrabun).



## [1.3.1] - 2019-12-19
### Changed

	- Fixed wrong stick drop (2 instead of 1) for Minetest Game and More Ores' swords.



## [1.3.0] - 2019-12-16
### Added

	- Default tool break sound.



## [1.2.0] - 2019-10-25
### Added

	- Advanced options: scraps' cooktimes, scraps' amounts.
	- Global function to allow third party mods' developers to add
		their own tools (see api.txt).
	- Local variables and tables get flushed once used for memory saving.

### Changed

	- Code entirely rewritten.
	- Scraps get spawned 1 node higher than player's character position
		to prevent them being "eaten" by other nodes (e.g. snow, grass).


## [1.1.0] - 2019-10-15
### Added

	- Support for More Ores' tools

### Changed

	- Fixed wrong conditional for Toolranks.
	- Fixed wrong drop values for the default MTG's shovels.



## [1.0.1] - 2019-10-01
### Changed

	- Overriders will not work if Toolranks is installed.



## [1.0.0] - 2019-10-01
### Added

	- Scrap materials that can be smelt into ingots, crystals and diamonds.

### Changed

	- Broken tools will turn into wooden sticks and drop scrap materials.

### Removed

	- Custom broken tools replacers.



## [0.1.0] - 2019-09-30
### Added

	- Initial release
