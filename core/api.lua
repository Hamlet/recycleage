--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Global mod namespace

recycleage = {}

-- Constants
local i_DEFAULT_AMOUNT = 2
local f_DEFAULT_OFFSET = 1.0

-- Variables

local s_ReplacerHeld, s_ReplacerDropped = nil

--
-- Global function: after_use overrider.
--

--[[
	"Tools use a wear (damage) value ranging from 0 to 65535.
	The   value 0 is the default and is used for unworn tools.
	The values 1 to 65535 are used for worn tools, where a
	higher value stands for   a higher wear.
	Non-tools always have a wear value of 0."
	Source: lua_api.txt at line 1352 (Minetest v5.0.1)
--]]

recycleage.after_use_overrider = function(itemstring,
	material_1, material_1_amount, material_2, material_2_amount)

	local s_ItemName = tostring(itemstring)

	-- Create an entry for the item in the global table, having
	-- two fields.
	recycleage[s_ItemName] = {s_ReplacerHeld, s_ReplacerDropped}

	-- Set the first field accordingly.
	if (material_1 ~= nil) then
		if (material_1_amount ~= nil) then
			recycleage[s_ItemName].s_ReplacerHeld =
				material_1 .. ' ' .. material_1_amount

		else
			recycleage[s_ItemName].s_ReplacerHeld =
				material_1 .. ' ' .. i_DEFAULT_AMOUNT

		end
	end

	-- Set the second field accordingly.
	if (material_2 ~= nil) then
		if (material_2_amount ~= nil) then
			recycleage[s_ItemName].s_ReplacerDropped =
				material_2 .. ' ' .. material_2_amount

		else
			recycleage[s_ItemName].s_ReplacerDropped =
				material_2 .. ' ' .. i_DEFAULT_AMOUNT

		end
	end

	--[[
	print('==============================================================');
	print('Item name: ' .. itemstring);
	print('Replacer held: ' .. dump(recycleage[s_ItemName].s_ReplacerHeld));
	print('Replacer dropped: ' ..
		dump(recycleage[s_ItemName].s_ReplacerDropped));
	--]]

	-- Actually override the registered item.
	minetest.override_item(itemstring, {

		--[[
		print('Re-Cycle Age - Overriding: ' .. itemstring);
		print('==============================================================');
		--]]

		after_use = function(itemstack, user, node, digparams)
			local i_toolWear = itemstack:get_wear()
			local i_nodeWear = digparams.wear

			itemstack:add_wear(digparams.wear)

			if ((i_toolWear + i_nodeWear) <= 65535) then
				--print('Re-Cycle Age - Case A')
				return itemstack

			else
				--print('Re-Cycle Age - Case B');

				local t_Position = nil

				minetest.sound_play('default_tool_breaks', {
					to_player = user:get_player_name()
				})

				-- Add replacer to inventory and spawn inworld
				if (recycleage[s_ItemName].s_ReplacerHeld ~= nil)
				and (recycleage[s_ItemName].s_ReplacerDropped ~= nil)
				then
					t_Position = user:get_pos()
					t_Position.y = (t_Position.y + f_DEFAULT_OFFSET)

					itemstack:replace(recycleage[s_ItemName].s_ReplacerHeld)
					minetest.add_item(t_Position,
						recycleage[s_ItemName].s_ReplacerDropped)

				-- Add replacer to inventory
				elseif (recycleage[s_ItemName].s_ReplacerHeld ~= nil)
				and (recycleage[s_ItemName].s_ReplacerDropped == nil)
				then
					itemstack:replace(recycleage[s_ItemName].s_ReplacerHeld)

				-- Spawn replacer inworld
				elseif (recycleage[s_ItemName].s_ReplacerHeld == nil)
				and (recycleage[s_ItemName].s_ReplacerDropped ~= nil)
				then
					t_Position = user:get_pos()
					t_Position.y = (t_Position.y + f_DEFAULT_OFFSET)

					minetest.add_item(t_Position,
						recycleage[s_ItemName].s_ReplacerDropped)
				end

				return itemstack
			end
		end
	})

	-- Flush the variables
	s_ReplacerHeld, s_ReplacerDropped = nil
end
