--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Scrap materials' cooking recipes for Minetest Game v5.x
--

minetest.register_craft({
	type = "cooking",
	output = "default:bronze_ingot",
	recipe = "recycleage:scrap_metal_bronze",
	cooktime = minetest.settings:get("recycleage_scraps_bronze_cooktime") or 5
})

minetest.register_craft({
	type = "cooking",
	output = "default:steel_ingot",
	recipe = "recycleage:scrap_metal_steel",
	cooktime = minetest.settings:get("recycleage_scraps_steel_cooktime") or 7
})

minetest.register_craft({
	type = "cooking",
	output = "default:mese_crystal",
	recipe = "recycleage:scrap_shards_mese",
	cooktime = minetest.settings:get("recycleage_shards_mese_cooktime") or 185
})

minetest.register_craft({
	type = "cooking",
	output = "default:diamond",
	recipe = "recycleage:scrap_shards_diamond",
	cooktime = minetest.settings:get("recycleage_shards_diamond_cooktime") or 370
})

minetest.register_craft({
	type = "cooking",
	output = "default:obsidian_shard",
	recipe = "recycleage:scrap_shards_obsidian",
	cooktime = minetest.settings:get("recycleage_shards_obsidian_cooktime") or 370
})

minetest.register_craft({
	type = "cooking",
	output = "default:gold_ingot",
	recipe = "recycleage:scrap_metal_gold",
	cooktime = minetest.settings:get("recycleage_scraps_gold_cooktime") or 7
})


--
-- Scrap materials' cooking recipes for More Ores
--

if (minetest.get_modpath("moreores") ~= nil) then

	minetest.register_craft({
		type = "cooking",
		output = "moreores:silver_ingot",
		recipe = "recycleage:scrap_metal_silver",
		cooktime = minetest.settings:get("recycleage_scraps_silver_cooktime") or 6
	})

	minetest.register_craft({
		type = "cooking",
		output = "moreores:mithril_ingot",
		recipe = "recycleage:scrap_metal_mithril",
		cooktime = minetest.settings:get("recycleage_scraps_mithril_cooktime") or 555
	})

end
