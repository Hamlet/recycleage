--[[

	Copyright © 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Local variables
--

-- Default drop amounts per tool's type

local i_DropPick =		minetest.settings:get("recycleage_drop_pick") or 2
local i_DropShovel =	minetest.settings:get("recycleage_drop_shovel") or 0
local i_DropAxe =		minetest.settings:get("recycleage_drop_axe") or 2
local i_DropSword =		minetest.settings:get("recycleage_drop_sword") or 1
local i_DropHoe =		minetest.settings:get("recycleage_drop_hoe") or 1

-- Used if More Ores is enabled

local t_MoreOresTools = {}
local t_MoreOresSwords = {}


--
-- Local function
--

-- Used to determine which scrap s_Material should be used.

local DefineScraps = function(a_s_itemstring)
	local s_Material = nil

	if (string.match(a_s_itemstring, "stone") ~= nil) then
		s_Material = "default:cobblestone"

	elseif (string.match(a_s_itemstring, "bronze") ~= nil) then
		s_Material = "recycleage:scrap_metal_bronze"

	elseif (string.match(a_s_itemstring, "steel") ~= nil) then
		s_Material = "recycleage:scrap_metal_steel"

	elseif (string.match(a_s_itemstring, "mese") ~= nil) then
		s_Material = "recycleage:scrap_shards_mese"

	elseif (string.match(a_s_itemstring, "diamond") ~= nil) then
		s_Material = "recycleage:scrap_shards_diamond"

	end

	if (minetest.get_modpath("moreores") ~= nil) then
		if (string.match(a_s_itemstring, "mithril") ~= nil) then
			s_Material = "recycleage:scrap_metal_mithril"

		elseif (string.match(a_s_itemstring, "silver") ~= nil) then
			s_Material = "recycleage:scrap_metal_silver"
		end
	end

	return s_Material
end


--
-- Default Minetest Game v5.x tools
--

local t_MTGtools = {

	-- Wooden
	{"default:pick_wood",		i_DropPick},
	{"default:shovel_wood",		i_DropShovel},
	{"default:axe_wood",		i_DropAxe},
	{"farming:hoe_wood",		i_DropHoe},

	-- Stone
	{"default:pick_stone",		i_DropPick},
	{"default:shovel_stone",	i_DropShovel},
	{"default:axe_stone",		i_DropAxe},
	{"farming:hoe_stone",		i_DropHoe},

	-- Bronze
	{"default:pick_bronze",		i_DropPick},
	{"default:shovel_bronze",	i_DropShovel},
	{"default:axe_bronze",		i_DropAxe},
	{"farming:hoe_bronze",		i_DropHoe},

	-- Steel
	{"default:pick_steel",		i_DropPick},
	{"default:shovel_steel",	i_DropShovel},
	{"default:axe_steel",		i_DropAxe},
	{"farming:hoe_steel",		i_DropHoe},

	-- Mese
	{"default:pick_mese",		i_DropPick},
	{"default:shovel_mese",		i_DropShovel},
	{"default:axe_mese",		i_DropAxe},
	{"farming:hoe_mese",		i_DropHoe},

	-- Diamond
	{"default:pick_diamond",	i_DropPick},
	{"default:shovel_diamond",	i_DropShovel},
	{"default:axe_diamond",		i_DropAxe},
	{"farming:hoe_diamond",		i_DropHoe}
}

-- Weapons

local t_MTGswords = {
	{"default:sword_wood",		i_DropSword},
	{"default:sword_stone",		i_DropSword},
	{"default:sword_bronze",	i_DropSword},
	{"default:sword_steel",		i_DropSword},
	{"default:sword_mese",		i_DropSword},
	{"default:sword_diamond",	i_DropSword}
}


--
-- More Ores' tools
--

if (minetest.get_modpath("moreores") ~= nil) then
	t_MoreOresTools = {

		-- Mithril
		{"moreores:pick_mithril",	i_DropPick},
		{"moreores:shovel_mithril",	i_DropShovel},
		{"moreores:axe_mithril",	i_DropAxe},
		{"moreores:hoe_mithril",	i_DropHoe},

		-- Silver
		{"moreores:pick_silver",	i_DropPick},
		{"moreores:shovel_silver",	i_DropShovel},
		{"moreores:axe_silver",		i_DropAxe},
		{"moreores:hoe_silver",		i_DropHoe}
	}

	t_MoreOresSwords = {
		{"moreores:sword_mithril",	i_DropSword},
		{"moreores:sword_silver",	i_DropSword}
	}
end


--
-- Tools' overriders
--

-- Minetest Game v5.x tools

for _, value in ipairs(t_MTGtools) do
	local s_Tool = value[1]
	local i_HeadPieces = value[2]
	local s_Handle = "default:stick"
	local s_HandlePieces = 1
	local s_Scraps = DefineScraps(s_Tool)

	recycleage.after_use_overrider(s_Tool, s_Handle, s_HandlePieces,
		s_Scraps, i_HeadPieces)
end


-- More Ores' tools

if (minetest.get_modpath("moreores") ~= nil) then
	for _, value in ipairs(t_MoreOresTools) do
		local s_Tool = value[1]
		local i_HeadPieces = value[2]
		local s_Handle = "default:stick"
		local s_HandlePieces = 1
		local s_Scraps = DefineScraps(s_Tool)

		recycleage.after_use_overrider(s_Tool, s_Handle, s_HandlePieces,
			s_Scraps, i_HeadPieces)
	end
end


--
-- Weapons' overriders
--

-- Minetest Game v5.x swords

for _, value in ipairs(t_MTGswords) do
	local s_Tool = value[1]
	local i_HeadPieces = value[2]
	local s_Handle = "default:stick"
	local s_HandlePieces = 0
	local s_Scraps = DefineScraps(s_Tool)

	recycleage.after_use_overrider(s_Tool, s_Handle, s_HandlePieces,
		s_Scraps, i_HeadPieces)
end


-- More Ores's swords

if (minetest.get_modpath("moreores") ~= nil) then
	for _, value in ipairs(t_MoreOresSwords) do
		local s_Tool = value[1]
		local i_HeadPieces = value[2]
		local s_Handle = "default:stick"
		local s_HandlePieces = 0
		local s_Scraps = DefineScraps(s_Tool)

		recycleage.after_use_overrider(s_Tool, s_Handle, s_HandlePieces,
			s_Scraps, i_HeadPieces)
	end
end


--
-- Clear the variables and tables' contents for memory saving.
--

-- Variables

i_DropPick = nil
i_DropShovel = nil
i_DropAxe = nil
i_DropSword = nil
i_DropHoe = nil

-- Tables

t_MTGtools = nil
t_MTGswords = nil
t_MoreOresTools = nil
t_MoreOresSwords = nil
